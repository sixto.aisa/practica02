import MongoMovimientoRepository from './infraestructure/MongoMovimientoRepository'
import MongoCuentaRepository from '../cuentas/infraestructure/MongoCuentaRepository'
import MongoEntidadRepository from '../entidades/infraestructure/MongoEntidadRepository'

import CrearMovimiento from './application/crearMovimiento'
import GetMovimientos from './application/getMovimientos'
// ENTIDAD
import GetEntidad from '../entidades/application/getEntidad'
import GetCuenta from '../cuentas/application/getCuenta'
import GetCuentas from '../cuentas/application/getCuentas'
import ActualizarCuenta from '../cuentas/application/actualizarCuenta'

const CuentaRepository = new MongoCuentaRepository()
const MovimientoRepository = new MongoMovimientoRepository()
const EntidadRepository = new MongoEntidadRepository()
/**
 * @param {import('express').Request} _
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
export const deposito = async (req, res, next) => {
  try {
    const queryCrearMovimiento = CrearMovimiento({ MovimientoRepository })
    const queryGetCuenta = GetCuenta({ CuentaRepository })
    const queryActualizarCuenta = ActualizarCuenta({ CuentaRepository })
    const queryGetEntidad = GetEntidad({ EntidadRepository })

    const numeroDocumento = {
      numeroDocumento: req.body.numeroDocumento
    }

    const entidad = await queryGetEntidad(numeroDocumento)

    if (entidad == null) {
      res.status(403).json({
        message: 'El numero de documento no corresponde a ninguna entidad'
      })
    }

    const numeroCuenta = {
      identidad: entidad._id,
      numeroCuenta: req.body.numeroCuenta
    }

    const cuenta = await queryGetCuenta(numeroCuenta)
    if (cuenta == null) {
      res.status(403).json({
        message: 'El numero de cuenta ingresada no corresponde al numero de documento'
      })
    }
    const cuentaActualizada = {
      saldo: cuenta.saldo + req.body.importe
    }
    const nuevoMovimiento = {
      idCuentaOrigen: cuenta._id,
      tipo: 'DEPOSITO',
      importe: req.body.importe,
      idCuentaDestino: cuenta._id
    }

    await queryActualizarCuenta(cuenta._id, cuentaActualizada)
    const movimiento = await queryCrearMovimiento(nuevoMovimiento)

    res.status(201).json({
      data: movimiento,
      message: 'deposito realizado'
    })
  } catch (e) {
    next(e)
  }
}

export const retiro = async (req, res, next) => {
  try {
    const queryCrearMovimiento = CrearMovimiento({ MovimientoRepository })
    const queryGetCuenta = GetCuenta({ CuentaRepository })
    const queryActualizarCuenta = ActualizarCuenta({ CuentaRepository })
    const queryGetEntidad = GetEntidad({ EntidadRepository })

    const numeroDocumento = {
      numeroDocumento: req.body.numeroDocumento
    }

    const entidad = await queryGetEntidad(numeroDocumento)

    if (entidad == null) {
      res.status(403).json({
        message: 'El numero de documento no corresponde a ninguna entidad'
      })
    }

    const numeroCuenta = {
      identidad: entidad._id,
      numeroCuenta: req.body.numeroCuenta
    }

    const cuenta = await queryGetCuenta(numeroCuenta)
    if (cuenta == null) {
      res.status(400).json({
        message: 'El numero de cuenta ingresada no corresponde al numero de documento'
      })
    }

    if (req.body.importe > cuenta.saldo) {
      res.status(403).json({
        message: 'El saldo de la cuenta es insuficiente para realizar esta operacion'
      })
    }

    const cuentaActualizada = {
      saldo: cuenta.saldo - req.body.importe
    }

    const nuevoMovimiento = {
      idCuentaOrigen: cuenta._id,
      tipo: 'RETIRO',
      importe: req.body.importe,
      idCuentaDestino: cuenta._id
    }

    await queryActualizarCuenta(cuenta._id, cuentaActualizada)
    const movimiento = await queryCrearMovimiento(nuevoMovimiento)

    res.status(201).json({
      data: movimiento,
      message: 'retiro realizado'
    })
  } catch (e) {
    next(e)
  }
}

export const transferencia = async (req, res, next) => {
  try {
    const queryMovimiento = CrearMovimiento({ MovimientoRepository })
    const queryCuenta = GetCuenta({ CuentaRepository })
    const queryActualizarCuenta = ActualizarCuenta({ CuentaRepository })
    const queryEntidad = GetEntidad({ EntidadRepository })

    const numeroDocumento = {
      numeroDocumento: req.body.numeroDocumento
    }

    const entidad = await queryEntidad(numeroDocumento)
    if (entidad == null) {
      res.status(403).json({
        message: 'El numero de documento no corresponde a ninguna entidad'
      })
    }

    const numeroCuentaOrigen = {
      identidad: entidad._id,
      numeroCuenta: req.body.numeroCuentaOrigen
    }
    const numeroCuentaDestino = {
      identidad: entidad._id,
      numeroCuenta: req.body.numeroCuentaDestino
    }
    const cuentaOrigen = await queryCuenta(numeroCuentaOrigen)
    const cuentaDestino = await queryCuenta(numeroCuentaDestino)

    if (cuentaOrigen == null) {
      res.status(400).json({
        message: 'El numero de cuenta Origen no corresponde al numero de documento ingresado'
      })
    }

    if (cuentaDestino == null) {
      res.status(400).json({
        message: 'El numero de cuenta Destino no corresponde al numero de documento ingresado'
      })
    }

    if (cuentaOrigen._id.equals(cuentaDestino._id)) {
      res.status(400).json({
        message: 'Las cuentas de origen y destino deben ser diferentes'
      })
    }

    if (req.body.importe > cuentaOrigen.saldo) {
      res.status(403).json({
        message: 'El saldo de la cuenta Origen es insuficiente para realizar esta operacion'
      })
    }

    const cuentaOrigenActualizada = {
      saldo: cuentaOrigen.saldo - req.body.importe
    }

    const cuentaDestinoActualizada = {
      saldo: cuentaDestino.saldo - req.body.importe
    }

    const nuevoMovimiento = {
      idcuentaOrigen: cuentaOrigen._id,
      tipo: 'TRANSFERENCIA',
      importe: req.body.importe,
      idcuentaDestino: cuentaDestino._id
    }

    await queryActualizarCuenta(cuentaOrigen._id, cuentaOrigenActualizada)
    await queryActualizarCuenta(cuentaDestino._id, cuentaDestinoActualizada)

    const movimiento = await queryMovimiento(nuevoMovimiento)

    res.status(201).json({
      data: movimiento,
      message: 'transferencia realizada'
    })
  } catch (e) {
    next(e)
  }
}

export const movimientos = async (req, res, next) => {
  try {
    const queryGetMovimientos = GetMovimientos({ MovimientoRepository })
    const queryGetCuentas = GetCuentas({ CuentaRepository })
    const queryGetEntidad = GetEntidad({ EntidadRepository })

    const numeroDocumento = {
      numeroDocumento: req.params.numeroDocumento
    }

    const entidad = await queryGetEntidad(numeroDocumento)

    if (entidad == null) {
      res.status(403).json({
        message: 'El numero de documento no corresponde a ninguna entidad'
      })
    }

    const parametro1 = {
      identidad: entidad._id
    }

    const listaCuentas = await queryGetCuentas(parametro1)

    const listaRetorno = []

    for (const cuenta of listaCuentas) {
      const parametro2 = {
        idCuentaOrigen: cuenta._id
      }

      const listaMovimiento = await queryGetMovimientos(parametro2)

      for (const movimiento of listaMovimiento) {
        const retorno = {
          nombre: entidad.nombre,
          idCuenta: cuenta._id,
          numeroCuenta: cuenta.numeroCuenta,
          fecha: movimiento.fecha,
          tipo: movimiento.tipo,
          importe: movimiento.importe
        }

        listaRetorno.push(retorno)
      }
    }

    res.status(201).json({
      data: listaRetorno,
      message: 'Movimientos de Cuenta'
    })
  } catch
  (e) {
    next(e)
  }
}
