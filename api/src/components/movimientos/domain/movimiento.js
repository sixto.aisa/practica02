import Joi from 'joi'

export const createMovimientoDepositoRetiroSchema = Joi.object({
  numeroDocumento: Joi.string().required(),
  numeroCuenta: Joi.string().required(),
  importe: Joi.number().required(),
})

export const createMovimientoTransferenciaSchema = Joi.object({
  numeroDocumento: Joi.string().required(),
  numeroCuentaOrigen: Joi.string().required(),
  importe: Joi.number().required(),
  numeroCuentaDestino: Joi.string().required()
})
