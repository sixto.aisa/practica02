import MongoLib from '../../../lib/mongo'

class MongoEntidadRepository {
  constructor () {
    this.collection = 'movimiento'
    this.mongoDB = new MongoLib()
  }

  async add (entidad) {
    const id = await this.mongoDB.create(this.collection, entidad)
    return { id, ...entidad }
  }

  async getByIdcuenta ( {idCuentaOrigen} ) {
    return await this.mongoDB.getAll(this.collection,{idCuentaOrigen})
  }

  async getAll () {
    const query = null
    return this.mongoDB.getAll(this.collection, query)
  }
}

export default MongoEntidadRepository
