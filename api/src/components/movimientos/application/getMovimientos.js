
/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoUserRepository')} obj.UserRepository
 */
export default ({ MovimientoRepository }) => {
  return async (params) => { // parameters
    // verify parameters
    return await MovimientoRepository.getByIdcuenta(params)
    // notify?
  }
}
