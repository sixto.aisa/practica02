/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoMovimientoRepository')} obj.PersonRepository
 */
export default ({ MovimientoRepository }) => {
  return async ({ idCuentaOrigen, tipo, importe, idCuentaDestino }) => {
    const movimiento = {
      idCuentaOrigen: idCuentaOrigen,
      fecha: new Date().toISOString(),
      tipo: tipo,
      importe: importe,
      idCuentaDestino: idCuentaDestino
    }
    return await MovimientoRepository.add(movimiento)
  }
}
