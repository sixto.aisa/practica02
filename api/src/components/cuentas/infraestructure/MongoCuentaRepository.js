import MongoLib from '../../../lib/mongo'

class MongoEntidadRepository {
  constructor () {
    this.collection = 'cuenta'
    this.mongoDB = new MongoLib()
  }

  async add (entidad) {
    const id = await this.mongoDB.create(this.collection, entidad)
    return { id, ...entidad }
  }

  async update (id, data) {
    return this.mongoDB.update(this.collection, id, data)
  }

  async getByNumeroCuenta ({ identidad, numeroCuenta }) {
    return await this.mongoDB.get(this.collection, null, { identidad, numeroCuenta })
  }

  async getByIdEntidad ({ identidad }) {
    return await this.mongoDB.getAll(this.collection, { identidad })
  }

  async getAll () {
    const query = null
    return this.mongoDB.getAll(this.collection, query)
  }
}

export default MongoEntidadRepository
