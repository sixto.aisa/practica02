import MongoCuentaRepository from './infraestructure/MongoCuentaRepository'
import MongoEntidadRepository from '../entidades/infraestructure/MongoEntidadRepository'

import CrearCuenta from './application/crearCuenta'
import GetCuenta from './application/getCuenta'
// ENTIDAD
import GetEntidad from '../entidades/application/getEntidad'
import GetCuentas from './application/getCuentas'

const CuentaRepository = new MongoCuentaRepository()
const EntidadRepository = new MongoEntidadRepository()

/**
 * @param {import('express').Request} _
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
export const crearCuenta = async (req, res, next) => {
  try {
    const queryEntidad = GetEntidad({ EntidadRepository })
    const queryCuenta = CrearCuenta({ CuentaRepository })

    const entidad = await queryEntidad(req.body)
    const nuevaCuenta = {
      identidad: entidad._id,
      numeroCuenta: req.body.numeroCuenta
    }

    const cuenta = await queryCuenta(nuevaCuenta)

    res.status(201).json({
      data: cuenta,
      message: 'cuenta creada'
    })
  } catch (e) {
    next(e)
  }
}

export const allCuenta = async (req, res, next) => {
  try {
    const query = CrearCuenta({ CuentaRepository })
    const cuenta = await query(req.body)
    res.status(201).json({
      data: cuenta,
      message: 'created'
    })
  } catch (e) {
    next(e)
  }
}

export const getCuentas = async (req, res, next) => {
  try {
    const queryGetEntidad = GetEntidad({ EntidadRepository })
    const queryGetCuentas = GetCuentas({ CuentaRepository })

    const entidad = await queryGetEntidad(req.params)

    if (entidad == null) {
      res.status(403).json({
        message: 'El numero de documento no corresponde a ninguna entidad'
      })
    }

    const parametro = {
      identidad: entidad._id
    }

    const listaCuentas = await queryGetCuentas(parametro)
    const listaRetorno = []
    for (const cuenta of listaCuentas) {
      const retorno = {
        numeroCuenta: cuenta.numeroCuenta,
        saldo: cuenta.saldo
      }

      listaRetorno.push(retorno)
    }

    res.status(200).json({
      nombre: entidad.nombre,
      data: listaRetorno,
      message: 'Saldo de Cuenta'
    })
  } catch (e) {
    next(e)
  }
}
