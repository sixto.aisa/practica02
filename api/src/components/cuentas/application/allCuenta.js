/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoCuentaRepository')} obj.PersonRepository
 */
export default ({ CuentaRepository }) => {
  return async () => {
    return await CuentaRepository.getAll()
  }
}
