/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoCuentaRepository')} obj.PersonRepository
 */
export default ({ CuentaRepository }) => {
  return async (id, data) => {
    return await CuentaRepository.update(id, data)
  }
}
