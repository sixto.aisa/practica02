
/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoUserRepository')} obj.UserRepository
 */
export default ({ CuentaRepository }) => {
  return async (params) => { // parameters
    // verify parameters
    return await CuentaRepository.getByIdEntidad(params)
    // notify?
  }
}
