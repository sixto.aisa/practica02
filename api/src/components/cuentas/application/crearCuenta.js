/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoCuentaRepository')} obj.PersonRepository
 */
export default ({ CuentaRepository }) => {
  return async ({ identidad, numeroCuenta }) => {
    const cuenta = {
      identidad: identidad,
      numeroCuenta: numeroCuenta,
      saldo: 0
    }
    return await CuentaRepository.add(cuenta)
  }
}
