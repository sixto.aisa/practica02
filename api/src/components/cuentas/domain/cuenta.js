import Joi from 'joi'

export const createCuentaSchema = Joi.object({
  numeroDocumento: Joi.string().required(),
  numeroCuenta: Joi.string().required()
})
