import MongoEntidadRepository from './infraestructure/MongoEntidadRepository'
import MongoCuentaRepository from '../cuentas/infraestructure/MongoCuentaRepository'

import CrearEntidad from './application/crearEntidad'
import AllEntidades from './application/allEntidad'
import GetEntidad from './application/getEntidad'
import GetCuentas from '../cuentas/application/getCuentas'

const EntidadRepository = new MongoEntidadRepository()
const CuentaRepository = new MongoCuentaRepository()
/**
 * @param {import('express').Request} _
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
export const crearEntidad = async (req, res, next) => {
  try {
    const query = CrearEntidad({ EntidadRepository })
    const entidad = await query(req.body)
    res.status(201).json({
      data: entidad,
      message: 'created'
    })
  } catch (e) {
    next(e)
  }
}

export const allEntidades = async (req, res, next) => {
  try {
    const query = AllEntidades({ EntidadRepository })
    const entidades = await query()
    res.status(200).json({
      data: entidades,
      message: 'Todas las Entidades'
    })
  } catch (e) {
    next(e)
  }
}

export const getEntidad = async (req, res, next) => {
  try {
    const queryGetEntidad = GetEntidad({ EntidadRepository })
    const queryGetCuentas = GetCuentas({ CuentaRepository })

    const entidad = await queryGetEntidad(req.params)
    if (entidad == null) {
      res.status(403).json({
        message: 'El numero de documento no corresponde a ninguna entidad'
      })
    }

    const parametro = {
      identidad: entidad._id
    }

    const listaCuentas = await queryGetCuentas(parametro)
    let saldo = 0
    for (const cuenta of listaCuentas) {
      saldo = saldo + cuenta.saldo
    }

    res.status(200).json({
      nombre: entidad.nombre,
      saldo: saldo,
      message: 'Saldo General'
    })
  } catch (e) {
    next(e)
  }
}
