
/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoUserRepository')} obj.UserRepository
 */
export default ({ EntidadRepository }) => {
  return async (params) => { // parameters
    // verify parameters
    return await EntidadRepository.getByNumeroDocumento(params)
    // notify?
  }
}
