/**
 * @param {Object} obj
 * @param {import('../infraestructure/MongoEntidadRepository')} obj.PersonRepository
 */
export default ({ EntidadRepository }) => {
  return async ({ nombre, tipoDocumento, numeroDocumento }) => {
    const nuevaEntidad = {
      nombre: nombre,
      tipoDocumento: tipoDocumento,
      numeroDocumento: numeroDocumento
    }
    return await EntidadRepository.add(nuevaEntidad)
  }
}
