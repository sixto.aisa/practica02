import Joi from 'joi'

export const createEntidadSchema = Joi.object({
  nombre: Joi.string().required(),
  tipoDocumento: Joi.string().required(),
  numeroDocumento: Joi.string().required()
})
