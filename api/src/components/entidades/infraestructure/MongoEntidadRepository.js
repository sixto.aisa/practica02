import MongoLib from '../../../lib/mongo'

class MongoEntidadRepository {
  constructor () {
    this.collection = 'entidad'
    this.mongoDB = new MongoLib()
  }

  async add (entidad) {
    const id = await this.mongoDB.create(this.collection, entidad)
    return { id, ...entidad }
  }

  async getByNumeroDocumento ({ numeroDocumento }) {
    return await this.mongoDB.get(this.collection,null, { numeroDocumento })
  }

  async getAll () {
    const query = null
    return this.mongoDB.getAll(this.collection, query)
  }
}

export default MongoEntidadRepository
