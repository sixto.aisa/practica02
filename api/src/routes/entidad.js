import express from 'express'
import { crearEntidad, allEntidades, getEntidad } from '../components/entidades/controller'
import { createEntidadSchema } from '../components/entidades/domain/entidad'
import { validationHandler } from '../utils/middlewares/validationHandler'

const router = express.Router()

router.get('/', async (_, response, next) => {
  response.send('<h1>Hello World!</h1>')
})

router.get('/api/entidades', allEntidades)
router.get('/api/entidad/:numeroDocumento', getEntidad)
router.get('/api/entidad/movimientos/:numeroDocumento', getEntidad)

router.post('/api/entidades', validationHandler(createEntidadSchema), crearEntidad)

export default router
