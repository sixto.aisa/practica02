import express from 'express'
import { deposito, retiro, transferencia, movimientos } from '../components/movimientos/controller'
import { createMovimientoDepositoRetiroSchema, createMovimientoTransferenciaSchema } from '../components/movimientos/domain/movimiento'
import { validationHandler } from '../utils/middlewares/validationHandler'


const router = express.Router()

router.get('/', async (_, response, next) => {
  response.send('<h1>Hello World!</h1>')
})

router.get('/api/movimientos/:numeroDocumento', movimientos)


router.post('/api/movimientos/deposito', validationHandler(createMovimientoDepositoRetiroSchema), deposito)
router.post('/api/movimientos/retiro', validationHandler(createMovimientoDepositoRetiroSchema), retiro)
router.post('/api/movimientos/transferencia', validationHandler(createMovimientoTransferenciaSchema), transferencia)

export default router
