import express from 'express'
import { crearCuenta, getCuentas } from '../components/cuentas/controller'
import { createCuentaSchema } from '../components/cuentas/domain/cuenta'
import { validationHandler } from '../utils/middlewares/validationHandler'


const router = express.Router()

router.get('/', async (_, response, next) => {
  response.send('<h1>Hello World!</h1>')
})

router.get('/api/cuentas/:numeroDocumento', getCuentas)

router.post('/api/cuentas', validationHandler(createCuentaSchema), crearCuenta)

export default router
