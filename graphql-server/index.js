const {ApolloServer, gql} = require('apollo-server')
const {v4: uuid} = require('uuid')
const axios = require('axios').default;

let capitulos = []

const typeDefs = gql` 
  type Entidad {  
       nombre: String! 
       saldo: Float! 
  }
  type Trasferencia {  
       id: String!
       fecha: String!
       tipo: String!
       importe: Float!        
  }
  type Movimiento {  
       nombre: String!
       idCuenta: String!
       numeroCuenta: String!
       fecha: String! 
       tipo: String!       
       importe: Float!        
  }
  type Mutation {
    "Realiza una nueva transferencia"
    addTransferencia(
        numeroDocumento: String!    
        numeroCuentaOrigen: String!
        numeroCuentaDestino: String!
        importe: Float!   
    ): Trasferencia  
  }
  type Query {
   "saldo de una entidad"
    saldoEntidad(numeroDocumento: String): Entidad!  
    "movimientos que fueron realizados en las cuentas de una Entidad"
    movimientosCuenta(numeroDocumento: String): Movimiento!      
  }`

let urlEntidad = "http://localhost:3000/api/entidad"
let urlTransferencia = "http://localhost:3000/api/movimientos/transferencia"
let urlMovimientos = "http://localhost:3000/api/movimientos"

const resolvers = {
    Query: {
        saldoEntidad: (root, args) => {

            let retorno = axios(urlEntidad + "/" + args.numeroDocumento)
                .then((result) => {
                    return result.data

                })
                .catch((error) => {
                    return []
                });

            return retorno
        },
        movimientosCuenta: (root, args) => {
            let retorno = axios(urlMovimientos + "/" + args.numeroDocumento)
                .then((result) => {
                    return result.data.data

                })
                .catch((error) => {
                    console.log(error)
                    return []
                });

            console.log(retorno)

            return retorno
        }
    } ,
    Mutation: {
        addTransferencia: (root, args) => {
            const transferencia = {...args}

            let retorno = axios.post(urlTransferencia, transferencia).then((result) => {

                console.log(result.data.data)

                return result.data.data

            })
                .catch((error) => {
                    console.log(error)
                    return []
                });

            return retorno
        },
    },
}

const server = new ApolloServer({
    typeDefs,
    resolvers,
})

server.listen().then(({url}) => {
    console.log(`Server ready at ${url}`)
})
